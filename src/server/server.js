const express = require('express');
const cors = require('cors');
require('colors');
const fileUpload = require('express-fileupload');

const { createServer } = require('http');

const connectDb = require('../database/db.config');
const { socketController } = require('../sockets/socket.controller');


class Server {

    constructor() {

        // Inicializando servidor de express
        // Initializing the express server
        this.app = express();

        // Variables de entornos
        // Environment variables
        this.port = process.env.PORT;

        // Inicializar y crear el servidor con sokets
        this.server = createServer(this.app);
        this.io = require('socket.io')(this.server);

        // Paths de rutas : EndPoint
        // Paths of rutas : EndPoint
        this.path = {
            auth: '/api/auth',
            category: '/api/category',
            search: '/api/search',
            product: '/api/product',
            upload: '/api/upload',
            user: '/api/user'
        }

        // Inicializando Conexion a DB
        // Initializing DB Connection
        this.dbConnect();

        // Middlewares
        this.middlewares();

        // Inicializando rutas
        // Initializing routes
        this.routes();

        // Inicilizar sockets
        this.sockets();


    }

    // Methods

    // Conectando a db de mongo
    // Connecting to mongo db
    async dbConnect() {
        await connectDb();
    }

    // Middlewares
    middlewares() {

        // Cors
        // Permite las peticiones de cualquier URL
        this.app.use(cors());

        // Lectura y parseo de JSON
        // El manejo de la información en formato JSON
        this.app.use(express.json());

        // Carpeta publica estatica
        this.app.use(express.static('src/public'));

        // File upload
        // aceptar carga de archivo en la APIRest
        this.app.use(fileUpload({
            useTempFiles: true,
            tempFileDir: '/temp',
            createParentPath: true
        }));
    }

    // Endpoint
    routes() {

        this.app.use(this.path.auth, require('../routes/auth.routes'));
        this.app.use(this.path.category, require('../routes/category.routes'));
        this.app.use(this.path.product, require('../routes/product.routes'));
        this.app.use(this.path.search, require('../routes/search.routes'));
        this.app.use(this.path.upload, require('../routes/upload.routes'));
        this.app.use(this.path.user, require('../routes/user.routes'));
    }


    sockets() {
        // this.io.on('connect', socketController); // Sin pasarle la referencia del io
        this.io.on('connect', (socket) => socketController(socket, this.io)); // pasar la referencio io para emitir a todos y al cliente
    }

    // Escuchando al servidor
    // Listening to the server
    listener() {
        this.server.listen(this.port, () => {
            console.log(`Server Online in port: ${this.port.green}`);
        })
    }
}

module.exports = Server;