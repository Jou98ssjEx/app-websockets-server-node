const { Router } = require('express');
const { check } = require('express-validator');

const { getProducts, getProductById, createProduct, updateProduct, deleteProduct } = require('../controllers');
const { existProductById, existCategoryById } = require('../helpers');
const { validateContent, validateJwt } = require('../middlewares');

const router = Router();


router.get('/', getProducts);


router.get('/:id', [
    check('id', 'No es un Id de mongoDb').isMongoId(),
    check('id').custom(existProductById),
    validateContent
], getProductById);


router.post('/', [
    validateJwt,
    check('name', 'This name is required').not().isEmpty(),
    check('category', 'This category is required').not().isEmpty(),
    check('category').isMongoId(),
    check('category').custom(existCategoryById),
    // check('user'),
    validateContent
], createProduct);


router.put('/:id', [
    check('id', 'No es id de Mongo').isMongoId(),
    check('id').custom(existProductById),
    validateContent
], updateProduct);


router.delete('/:id', [
    validateJwt,
    check('id', 'No es id de Mongo').isMongoId(),
    check('id').custom(existProductById),
    validateContent
], deleteProduct);

module.exports = router