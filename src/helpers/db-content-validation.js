const { RolModel, UserModel, CategoryModel, ProductModel } = require('../models')

const validateRole = async(rol = '') => {
    const existRole = await RolModel.findOne({ rol });
    if (!existRole) {
        throw new Error(`El rol ${rol} no esta resgistrado en la DB`);
    }
}

const existEmail = async(email) => {

    const exits = await UserModel.findOne({ email });
    if (exits) {
        throw new Error(`El email ${email} ya esta registrado`);
    }
}

const existUserById = async(id) => {
    const userExist = await UserModel.findById(id);
    if (!userExist) {
        throw new Error(`El ${id}, no le pertence a ningun usuario`);
    }
}

const existCategoryById = async(id) => {
    const categoryExist = await CategoryModel.findById(id);
    if (!categoryExist) {
        throw new Error(`El ${id}, no le pertence a ninguna categoria`);
    }
}

const existProductById = async(id) => {
    const productExist = await ProductModel.findById(id);
    if (!productExist) {
        throw new Error(`El ${id}, no le pertence a ningun producto`);
    }
}

const permittedCollections = (collection = '', collections = []) => {
    const inclued = collections.includes(collection);

    if (!inclued) {
        throw new Error(`La coleccion: '${collection}' no es permitida, coleciones permitidas: '${collections }'`);
    }

    return true;
}

module.exports = {
    validateRole,
    existEmail,
    existUserById,
    existCategoryById,
    existProductById,
    permittedCollections
}