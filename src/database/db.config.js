const mongoose = require('mongoose');


const connectDb = async() => {
    const db = 'Production';
    try {
        await mongoose.connect(process.env.MONGO_DB);
        console.log(`Databe online in: ${db.green}`);
    } catch (error) {
        console.log(error);
        throw new Error('Error connecting the database');
    }
}

module.exports = connectDb;