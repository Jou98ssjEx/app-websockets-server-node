const { request, response } = require("express");
const { ObjectId } = require('mongoose').Types
const { UserModel, CategoryModel, ProductModel } = require("../models");

const colletionExists = [
    'user',
    'category',
    'product',
    'rol'
];

// Busqueda de user
const searchUser = async(term = '', res = response) => {

    const isMongoId = ObjectId.isValid(term); // true or false

    if (isMongoId) {
        const user = await UserModel.findById(term);
        return res.json({
            results: (user) ? [user] : []
        });
    }
    // Expresion relugar para hacer busquedas insensibles
    const regex = new RegExp(term, 'i');

    // BUsquedas por term
    const users = await UserModel.find({
        $or: [{ name: regex }, { email: regex }],
        $and: [{ status: true }]
    })

    res.json({
        found: users.length,
        results: users
    })
}

// Busqueda de categoria :name
const searchCategory = async(term = '', res = response) => {

    const isMongoId = ObjectId.isValid(term);

    if (isMongoId) {
        const category = await CategoryModel.findById(term).populate('user', 'name');
        return res.json({
            results: (category) ? [category] : []
        })
    }

    // Expresion regular insensible
    const regex = new RegExp(term, 'i');

    // busqueda por term
    const categories = await CategoryModel.find({
        $or: [{ name: regex }],
        $and: [{ status: true }]
    }).populate('user', 'name');

    res.json({
        found: categories.length,
        results: categories
    })

}

// Busqueda de producto :name
const searchProduct = async(term = '', res = response) => {

    const isMongoId = ObjectId.isValid(term);

    if (isMongoId) {
        const product = await ProductModel.findById(term)
            .populate('user', 'name')
            .populate('category', 'name')

        return res.json({
            results: (product) ? [product] : []
        })
    }

    const regex = new RegExp(term, 'i');

    const products = await ProductModel.find({
            $or: [{ name: regex }],
            $and: [{ status: true }]

        })
        .populate('user', 'name')
        .populate('category', 'name')

    res.json({
        found: products.length,
        results: products
    })
}

// Busquedas en la db
const searchesDb = async(req = request, res = response) => {

    const { collection, term } = req.params;

    if (!colletionExists.includes(collection)) {
        res.status(400).json({
            msg: `Las colecciones deben ser: ${colletionExists}`
        })
    }

    switch (collection) {
        case 'user':
            searchUser(term, res);
            break;
        case 'category':
            searchCategory(term, res);
            break;
        case 'product':
            searchProduct(term, res)
            break;
        case 'rol':

            break;

        default:
            res.status(500).json({
                msg: 'Verificar colleciones existentes'
            })
            break;
    }

}

module.exports = {
    searchesDb
}