const { Router } = require('express');
const { check } = require('express-validator');

const { login, googleSingIn, renewToken } = require('../controllers');
const { validateContent, validateJwt } = require('../middlewares');

const router = Router();

router.post('/login', [
    check('email', 'Email is required'),
    check('password', 'The password is required'),
    validateContent
], login);

router.post('/google', [
    check('token_google', 'Token is requered').not().isEmpty(),
    validateContent
], googleSingIn);

router.get('/', validateJwt, renewToken);

module.exports = router;