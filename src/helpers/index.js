const dbContentValidation = require('./db-content-validation');
const googleVerify = require('./google-verify');
const jsonWebtoken = require('./jsonWebToken');
const uploadFile = require('./upload-file');

module.exports = {
    ...dbContentValidation,
    ...googleVerify,
    ...jsonWebtoken,
    ...uploadFile,
}