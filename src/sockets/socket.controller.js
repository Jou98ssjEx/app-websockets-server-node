const { Socket } = require("socket.io");
const { checkToken } = require("../helpers/jsonWebToken");

const ChatMessages = require("../class/chat-message");
const chatMessages = new ChatMessages();

const socketController = async(socket = new Socket, io) => {

    const user = await checkToken(socket.handshake.headers['x-token']);

    if (!user) {
        return socket.disconnect();
    }

    // agregar el usuario conectado a la logica de chatMessages
    chatMessages.userConnect(user);
    console.log('');
    console.log('Conntect: ', user.name);

    // Emitir para todos los usuarios activos, que estan en chatMessages
    io.emit('user-active', chatMessages.userArr);

    // Emitir los ultimos mensajes
    io.emit('listen-message', chatMessages.lastUser10);

    //Conectarlo a una sala privada: mensaje privado
    socket.join(user.id); // GLobal, socket.id, user.id : tipo de mensajes

    // Limpiar cuando el usuario se desconecta
    socket.on('disconnect', () => {
        chatMessages.userDisconnect(user.id);
        // Volver a emitir los usuarios que estan activos
        io.emit('user-active', chatMessages.userArr);

    })

    socket.on('send-message', ({ uid, message }) => {
        // console.log(p);

        if (uid) {
            // envia un mensaje privado
            io.to(uid).emit('private-message', { de: user.name, message })
        } else {

            // Mensajes Globales
            chatMessages.sendMessage(user.id, user.name, message);
            io.emit('listen-message', chatMessages.lastUser10);
        }

    })
}

module.exports = {
    socketController
}