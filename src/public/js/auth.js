const url = (window.location.hostname.includes('localhost')) ?
    'http://localhost:3000/api/auth/' :
    'https://heroku';

const myForm = document.querySelector('form');


myForm.addEventListener('submit', evn => {

    // No permite que el navegador se recarga al hacer subnit
    evn.preventDefault();


    console.log(evn);

    const formData = {};

    // Se hace un barrido a todos los elemtos del form
    for (let ele of myForm.elements) {

        // si los input tienen algo, se inserta en el formDAta
        if (ele.name.length > 1) {
            formData[ele.name] = ele.value
        }
    }

    console.log(formData);

    fetch(url + 'login', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
    })

    .then(resp => resp.json())
        .then(({ msg, token }) => {

            if (msg) {
                console.error(msg);
            }
            console.log({ token });
            localStorage.setItem('token', token);

            // redireccionamiento
            window.location = 'chat.html';

        })
        .catch((err) => {
            console.log(err);
        });
})




function handleCredentialResponse(response) {

    // const responsePayload = decodeJwtResponse(response.credential);

    console.log('TOKEN', response.credential);

    // const body = response.credential;
    const body = {
        token_google: response.credential
    }

    fetch(url + 'google', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        }).then(resp => resp.json()) // lo que envio al backend
        .then(({ token }) => {
            console.log(token) // Lo que el backend me responde
            localStorage.setItem('token', token);

            // redireccionamiento
            window.location = 'chat.html';

        })
        // .then(resp => {
        //     console.log(resp) // Lo que el backend me responde
        //     localStorage.setItem('email', resp.user.email);
        // })
        .catch(console.warn)

    // console.log("ID: " + responsePayload.sub);
    // console.log('Full Name: ' + responsePayload.name);
    // console.log('Given Name: ' + responsePayload.given_name);
    // console.log('Family Name: ' + responsePayload.family_name);
    // console.log("Image URL: " + responsePayload.picture);
    // console.log("Email: " + responsePayload.email);
}

const button = document.querySelector('#googleSingUp');
button.addEventListener('click', () => {
    console.log('click');
    console.log(google.accounts.id);

    google.accounts.id.disableAutoSelect();

    google.accounts.id.revoke(localStorage.getItem('email'), done => {
        localStorage.clear();
        location.reload()
    })
})