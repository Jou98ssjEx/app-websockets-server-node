const { request, response } = require("express");
const path = require('path');
const fs = require('fs');

const cloudinary = require('cloudinary').v2;
cloudinary.config(process.env.CLOUDINARY_URL);

const { upload_Files } = require('../helpers');
const { UserModel, ProductModel } = require('../models');

// Subir un archivo
const uploadFile = async(req = request, res = response) => {

    try {
        // const nameFile = await upload_Files(req.files);
        // const nameFile = await upload_Files(req.files, ['txt', 'md'], 'nameFolder');
        const nameFile = await upload_Files(req.files, undefined, 'imgs');
        // console.log(nameFile);
        res.json({
            nameFile
        });

    } catch (error) {
        throw new Error('Error al subir la img')
    }
}

// Subir una img a cloudinary: se crea directamente la carpeta de la collecion
const uploadImgCloudinary = async(req = request, res = response) => {

    const { id, collection } = req.params;

    let model;

    switch (collection) {
        case 'user':
            model = await UserModel.findById(id);

            if (!model) {
                return res.status(400).json({
                    msg: `No existe usuario con ese id: ${id}`
                })
            }

            if (!model.status) {
                return res.status(400).json({
                    msg: `No existe usuario con ese id: ${id} - status`
                })
            }

            break;

        case 'product':

            model = await ProductModel.findById(id);

            if (!model) {
                return res.status(400).json({
                    msg: `No existe producto con ese id: ${id}`
                })
            }

            if (!model.status) {
                return res.status(400).json({
                    msg: `No existe producto con ese id: ${id} - status`
                })
            }
            break;

        default:

            return res.status(500).json({
                msg: `Verificar las collecfiones`
            })
    }

    // Limpiar imagenes previas, cada producto o usuario solo puede tener una img
    if (model.img) {
        const nameArr = model.img.split('/');
        const imgName = nameArr[nameArr.length - 1];

        const [id_path] = imgName.split('.');
        cloudinary.uploader.destroy(id_path)
    }

    // console.log(req.files.file);
    const { tempFilePath } = req.files.file;
    // console.log({ tempFilePath });
    const { secure_url } = await cloudinary.uploader.upload(tempFilePath);

    // const nameFile = await upload_Files(req.files, undefined, collection);

    model.img = secure_url;

    await model.save();

    res.json({
        msg: 'Update Imagen',
        // id,
        // collection,
        model
    })

}

// Subir una img en el servidor
const uploadImg = async(req = request, res = response) => {

    const { id, collection } = req.params;

    let model;

    switch (collection) {
        case 'user':
            model = await UserModel.findById(id);

            if (!model) {
                return res.status(400).json({
                    msg: `No existe usuario con ese id: ${id}`
                })
            }

            if (!model.status) {
                return res.status(400).json({
                    msg: `No existe usuario con ese id: ${id} - status`
                })
            }

            break;

        case 'product':

            model = await ProductModel.findById(id);

            if (!model) {
                return res.status(400).json({
                    msg: `No existe producto con ese id: ${id}`
                })
            }

            if (!model.status) {
                return res.status(400).json({
                    msg: `No existe producto con ese id: ${id} - status`
                })
            }
            break;

        default:

            return res.status(500).json({
                msg: `Verificar las collecfiones`
            })
    }


    // Limpiar imagenes previas, cada producto o usuario solo puede tener una img
    // pathImg.split('.');
    // let nameExt = pathImg[pathImg.length-1];
    if (model.img) {
        const pathImg = path.join(__dirname, '../upload', collection, model.img);
        // Si la imagen existe en el servidor
        if (fs.existsSync(pathImg)) {
            fs.unlinkSync(pathImg);
        }
    }

    try {

        // const nameFile = await upload_Files(req.files, ['png', 'jpg'], collection);
        const nameFile = await upload_Files(req.files, undefined, collection);

        model.img = nameFile;

        await model.save();

        res.json({
            msg: 'Update Imagen',
            // id,
            // collection,
            model
        })

    } catch (error) {

        // throw new Error(`Algo salio mal: ${error}`)
        res.status(500).json({
            msg: 'Algo sali mal',
            error
        });

    }
}

// Obtener la img de la coleccion
const showImg = async(req = request, res = response) => {

    const { id, collection } = req.params;

    let model;

    switch (collection) {
        case 'user':
            model = await UserModel.findById(id);

            if (!model) {
                return res.status(400).json({
                    msg: `No existe usuario con ese id: ${id}`
                })
            }

            if (!model.status) {
                return res.status(400).json({
                    msg: `No existe usuario con ese id: ${id} - status`
                })
            }

            break;

        case 'product':

            model = await ProductModel.findById(id);

            if (!model) {
                return res.status(400).json({
                    msg: `No existe producto con ese id: ${id}`
                })
            }

            if (!model.status) {
                return res.status(400).json({
                    msg: `No existe producto con ese id: ${id} - status`
                })
            }
            break;

        default:

            return res.status(500).json({
                msg: `Verificar las collecfiones`
            })
    }

    if (model.img) {
        const pathImg = path.join(__dirname, '../upload', collection, model.img);
        // Si la imagen existe en el servidor
        if (fs.existsSync(pathImg)) {
            // fs.unlinkSync(pathImg);
            return res.sendFile(pathImg);
        }

    }

    // Si no existe la img de la coleccion, se mando una img de no encontrada
    const pathImgNoFound = path.join(__dirname, '../assets/no-image.jpg');

    res.sendFile(pathImgNoFound)

}


module.exports = {
    uploadFile,
    uploadImgCloudinary,
    uploadImg,
    showImg
}