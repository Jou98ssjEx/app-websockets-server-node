const validateContent = require('./validate-content');
const validateFileUpload = require('./validate-file-upload');
const validateJwt = require('./validate-jwt');
const validateRol = require('./validate-rol');

module.exports = {
    ...validateContent,
    ...validateFileUpload,
    ...validateJwt,
    ...validateRol
}