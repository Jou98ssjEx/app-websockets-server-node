const { request, response } = require("express");
const bcryptjs = require('bcryptjs');

const { UserModel } = require('../models');

const query = { status: true };

const getUsers = async(req = request, res = response) => {

    const { limit = 5, desde = 0 } = req.query;

    const [totalRegister, users] = await Promise.all([
        UserModel.countDocuments(query),
        UserModel.find(query).skip(Number(desde)).limit(Number(limit))
    ])

    res.json({
        msg: 'GetUsers',
        totalRegister,
        users
    });
}

const getUserById = async(req = request, res = response) => {

    const { id } = req.params;
    const userFound = await UserModel.findById(id);

    if (!userFound.status) {
        return res.status(400).json({
            msg: `User doesn't exist`
        })
    }

    res.json({
        msg: 'GetUserById',
        userFound
    })
}

const createUser = async(req = request, res = response) => {

    const body = req.body;
    let user = new UserModel(body);

    // Encriptar la contraseña
    // Encrypt the password
    const salt = bcryptjs.genSaltSync(11);
    user.password = bcryptjs.hashSync(user.password, salt);

    // Guardar la data en la db
    // Save the data in the db
    await user.save();

    // response
    res.json({
        msg: 'Create User',
        user
    });
}

const updateUser = async(req = request, res = response) => {

    const id = req.params.id;
    const { _id, email, password, google, ...data } = req.body;

    // Validar contra la db
    // Validate against db
    if (password) {
        // Encriptar la contraseña
        // Encrypt the password
        const salt = bcryptjs.genSaltSync(11);
        data.password = bcryptjs.hashSync(password, salt);
    }

    const user = await UserModel.findByIdAndUpdate(id, data, { new: true });

    res.json({
        msg: 'Put',
        user
    });
}

const deleteUser = async(req = request, res = response) => {

    const { id } = req.params;

    const falseUser = await UserModel.findByIdAndUpdate(id, {
        status: false
    }, { new: true });

    const userAuthenticate = req.userAuth;

    res.json({
        msg: 'Delete',
        falseUser,
        userAuthenticate
        // deleteUser
    });
}

module.exports = {
    getUsers,
    getUserById,
    createUser,
    updateUser,
    deleteUser,

}